import java.io.File;

public class FicheroExiste extends Exception {

    private File file;

    public FicheroExiste(File file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "El fichero " + file.getName() + " ya existe";
    }
}

