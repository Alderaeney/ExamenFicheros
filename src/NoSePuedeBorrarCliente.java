public class NoSePuedeBorrarCliente extends Exception {

    private Cliente cliente;

    public NoSePuedeBorrarCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public String toString() {
        return "El cliente " + cliente.getId() + " no se puede borrar porque tiene articulos asociados a él.";
    }
}
