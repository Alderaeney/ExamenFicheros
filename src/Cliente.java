import java.util.ArrayList;

public class Cliente {

    private String id;
    private String nombre;
    private ArrayList<Articulo> articulos;

    public Cliente(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
        this.articulos = new ArrayList<>();
    }

    public Articulo retornarArticulo(String id){
        for (Articulo art :
                this.articulos) {
            if (art.getId().equalsIgnoreCase(id)) return art;
        }
        return null;
    }

    public void borrarArticulo(Articulo articulo){
        if (this.articulos.size() == 1){
            this.articulos.clear();
        } else {
            this.articulos.remove(articulo);
        }
    }

    @Override
    public String toString() {
        return "ID: " + this.id + " | Nombre: " + this.nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Articulo> getArticulos() {
        return articulos;
    }

    public void setArticulos(ArrayList<Articulo> articulos) {
        this.articulos = articulos;
    }
}
