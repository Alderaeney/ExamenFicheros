public class Aperitivo extends Articulo {

    private Embalaje tipo;

    public Aperitivo(String id, String nombre, double precio, String categoria, Embalaje tipo) {
        super(id, nombre, precio, categoria);
        this.tipo = tipo;
    }

    public Embalaje getTipo() {
        return tipo;
    }

    public void setTipo(Embalaje tipo) {
        this.tipo = tipo;
    }

    @Override
    public int calcularNumeroCajas() {
        return 1;
    }
}
