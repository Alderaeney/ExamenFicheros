public class ClienteYaExiste extends Exception {

    private Cliente cliente;

    public ClienteYaExiste(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public String toString() {
        return "El cliente " + cliente.getId() + " ya existe";
    }
}
