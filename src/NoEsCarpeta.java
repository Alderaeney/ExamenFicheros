import java.io.File;

public class NoEsCarpeta extends Exception {
    private File archivo;

    public NoEsCarpeta(File archivo) {
        this.archivo = archivo;
    }

    @Override
    public String toString() {
        return "El archivo " + archivo.getName() + " existe pero no es una carpeta";
    }
}
