public class Bebida extends Articulo {

    private double litros;

    public Bebida(String id, String nombre, double precio, String categoria, double litros) {
        super(id, nombre, precio, categoria);
        this.litros = litros;
    }



    public double getLitros() {
        return litros;
    }

    public void setLitros(double litros) {
        this.litros = litros;
    }

    @Override
    public int calcularNumeroCajas() {
        return (int) Math.round(this.litros / 10) > 0?(int) Math.round(this.litros / 10):1;
    }
}
