import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        int opcion = 1;
        String nombre, id, rutaFichero, rutaCarpeta;
        char tipo, sal, categoria;
        boolean salir;
        Cliente cliAct;
        double precio = 0, litros = 0, kilos = 0;
        Articulo artAct;

        System.out.print("Introduce la ruta al archivo: ");
        rutaFichero = scanner.nextLine();
        System.out.print("Introduce la ruta a la carpeta dónde generar la estructura de carpetas: ");
        rutaCarpeta = scanner.nextLine();

        GestionArticulos gestionArticulos = new GestionArticulos(new File(rutaFichero), new File(rutaCarpeta));
        do {
            salir = false;
            do {
                try{
                    menu();
                    opcion = scanner.nextInt();
                    salir = true;
                } catch (Exception e){
                    System.out.println("Introduce un numero.");
                    scanner.nextLine();
                }
            } while (!salir);
            switch (opcion){
                case 1:
                    do {
                        salir =false;
                        do {
                            try{
                                menuGestion();
                                opcion = scanner.nextInt();
                                salir = true;
                            } catch (Exception e){
                                System.out.println("Introduce un numero.");
                                scanner.nextLine();
                            }
                        } while (!salir);
                        switch (opcion){
                            case 1:
                                gestionArticulos.deCsvALista();
                                gestionArticulos.imprimirRegistros();
                                System.out.println("Articulos cargados correctamente.");
                                break;

                            case 2:
                                try {
                                    gestionArticulos.deListaACarpeta();
                                    System.out.println("Sistema de ficheros creado.");
                                } catch (NoEsCarpeta noEsCarpeta) {
                                    noEsCarpeta.printStackTrace();
                                }
                                break;

                            case 3:
                                gestionArticulos.deCarpetaACsv();
                                System.out.print("Archivo csv creado.");
                                break;

                            case 4:
                                gestionArticulos.mostrarDisco();
                                break;

                            case 5:
                                System.out.println("Volviendo atrás...");
                                break;

                            default:
                                System.out.println("Opción no reconocida.");
                                break;
                        }

                    } while (opcion != 5);
                    break;

                case 2:
                    do {
                        salir = false;
                        do {
                            try{
                                menuCliArt();
                                opcion = scanner.nextInt();
                                salir = true;
                            } catch (Exception e){
                                System.out.println("Introduce un numero.");
                                scanner.nextLine();
                            }
                        } while (!salir);
                        switch (opcion){
                            case 1:
                                System.out.print("Introduce el id del cliente: ");
                                scanner.nextLine();
                                id = scanner.nextLine();
                                System.out.print("Introduce el nombre del cliente: ");
                                nombre = scanner.nextLine();
                                try {
                                    gestionArticulos.anadirCliente(new Cliente(id, nombre));
                                } catch (ClienteYaExiste clienteYaExiste) {
                                    System.out.println(clienteYaExiste.toString());
                                }
                                break;

                            case 2:
                                gestionArticulos.imprimirClientes();
                                scanner.nextLine();
                                System.out.print("Introduce id del cliente: ");
                                id = scanner.nextLine();
                                if ((cliAct = gestionArticulos.retornarCliente(id)) != null){
                                    System.out.print("Introduce tipo de articulo a añadir (B - Bebidas, A - Aperitivos, V - Varios): ");
                                    categoria = scanner.next().charAt(0);
                                    if (categoria == 'B'){
                                        System.out.print("Introduce el id: ");
                                        scanner.nextLine();
                                        id = scanner.nextLine();
                                        System.out.print("Introduce el nombre: ");
                                        nombre = scanner.nextLine();
                                        salir = false;
                                        do {
                                            try{
                                                System.out.print("Introduce el precio: ");
                                                precio = scanner.nextDouble();
                                                salir = true;
                                            } catch (Exception e){
                                                System.out.println("Numero no válido.");
                                                scanner.nextLine();
                                            }
                                        } while (!salir);
                                        salir = false;
                                        do {
                                            try{
                                                System.out.print("Introduce los litros: ");
                                                litros = scanner.nextDouble();
                                                salir = true;
                                            } catch (Exception e){
                                                System.out.println("Numero no válido.");
                                                scanner.nextLine();
                                            }
                                        } while (!salir);
                                        Bebida bebida = new Bebida(id, nombre, precio, "Bebida", litros);
                                        cliAct.getArticulos().add(bebida);
                                    } else if (categoria == 'V'){
                                        System.out.print("Introduce el id: ");
                                        scanner.nextLine();
                                        id = scanner.nextLine();
                                        System.out.print("Introduce el nombre: ");
                                        nombre = scanner.nextLine();
                                        salir = false;
                                        do {
                                            try{
                                                System.out.print("Introduce el precio: ");
                                                precio = scanner.nextDouble();
                                                salir = true;
                                            } catch (Exception e){
                                                System.out.println("Numero no válido.");
                                                scanner.nextLine();
                                            }
                                        } while (!salir);
                                        salir = false;
                                        do {
                                            try{
                                                System.out.print("Introduce los kilos: ");
                                                kilos = scanner.nextDouble();
                                                salir = true;
                                            } catch (Exception e){
                                                System.out.println("Numero no válido.");
                                                scanner.nextLine();
                                            }
                                        } while (!salir);
                                        Vario vario = new Vario(id, nombre, precio, "Varios", kilos);
                                        cliAct.getArticulos().add(vario);
                                    } else if (categoria == 'A'){
                                        System.out.print("Introduce el id: ");
                                        scanner.nextLine();
                                        id = scanner.nextLine();
                                        System.out.print("Introduce el nombre: ");
                                        nombre = scanner.nextLine();
                                        salir = false;
                                        do {
                                            try{
                                                System.out.print("Introduce el precio: ");
                                                precio = scanner.nextDouble();
                                                salir = true;
                                            } catch (Exception e){
                                                System.out.println("Numero no válido.");
                                                scanner.nextLine();
                                            }
                                        } while (!salir);
                                        System.out.print("Introduce el tipo de embalaje (b - bolsa, v - break, l - lata): ");
                                        tipo = scanner.next().charAt(0);
                                        if (tipo == 'b'){
                                            Aperitivo aperitivo = new Aperitivo(id, nombre, precio, "Aperitivo", Embalaje.BOLSA);
                                            cliAct.getArticulos().add(aperitivo);
                                        } else if (tipo == 'v'){
                                            Aperitivo aperitivo = new Aperitivo(id, nombre, precio, "Aperitivo", Embalaje.BREAK);
                                            cliAct.getArticulos().add(aperitivo);
                                        } else if (tipo == 'l'){
                                            Aperitivo aperitivo = new Aperitivo(id, nombre, precio, "Aperitivo", Embalaje.LATA);
                                            cliAct.getArticulos().add(aperitivo);
                                        } else System.out.println("Tipo no reconocido.");
                                    } else System.out.println("No se ha introducido correctamente el tipo de articulo.");
                                } else System.out.println("Cliente no encontrado.");
                                gestionArticulos.imprimirRegistros();
                                break;

                            case 3:

                                break;

                            case 4:
                                gestionArticulos.imprimirClientes();
                                scanner.nextLine();
                                System.out.print("Introduce id del cliente: ");
                                id = scanner.nextLine();
                                if ((cliAct = gestionArticulos.retornarCliente(id)) != null){
                                    gestionArticulos.imprimirRegistros();
                                    System.out.print("Introduce el id del producto: ");
                                    id = scanner.nextLine();
                                    if ((artAct = cliAct.retornarArticulo(id)) != null){
                                        cliAct.borrarArticulo(artAct);
                                    } else System.out.println("Articulo no encontrado.");
                                } else System.out.println("Cliente no encontrado.");
                                break;

                            case 5:
                                gestionArticulos.imprimirClientes();
                                scanner.nextLine();
                                System.out.print("Introduce id del cliente: ");
                                id = scanner.nextLine();
                                if ((cliAct = gestionArticulos.retornarCliente(id)) != null){
                                    try {
                                        gestionArticulos.borrarCliente(cliAct);
                                        System.out.println("Cliente borrado exitosamente.");
                                    } catch (NoSePuedeBorrarCliente noSePuedeBorrarCliente) {
                                        System.out.println(noSePuedeBorrarCliente.toString());
                                    }
                                } else System.out.println("Cliente no encontrado");

                                break;

                            case 6:
                                System.out.println("Volviendo atrás...");
                                break;

                            default:
                                System.out.println("Opción no reconocida.");
                                break;
                        }
                    } while (opcion != 6);
                    break;

                case 0:
                    System.out.println("Saliendo...");
                    break;

                default:
                    System.out.println("Opción no reconocida.");
                    break;
            }

        } while (opcion != 0);

    }

    public static void menu() {
        System.out.print("1.- Menú gestión ficheros\n2.- Menú gestión clientes y articulos\n0.- Salir\n-> ");
    }

    public static void menuGestion() {
        System.out.print("1.- Cargar articulos en lista\n2.- Generar estructura de carpetas\n3.- Generar fichero de registro\n4.- Ver estructura de carpetas\n5.- Volver atrás\n-> ");
    }

    public static void menuCliArt() {
        System.out.print("1.- Crear cliente\n2.- Añadir articulo\n3.- Modificar articulo\n4.- Borrar articulo\n5.- Borrar cliente\n6.- Volver atrás\n-> ");
    }
}
