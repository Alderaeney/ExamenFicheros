import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class GestionArticulos {

    private File archivo;
    private File carpeta;
    private ArrayList<Cliente> clientes;

    public GestionArticulos(File archivo, File carpeta) {
        this.archivo = archivo;
        this.carpeta = carpeta;
        this.clientes = new ArrayList<>();
    }

    public void deCsvALista(){
        String linea;
        String[] datos;
        Cliente clAct = null;
        try {
            Scanner scn = new Scanner(this.archivo);
            while (scn.hasNext()){
                linea = scn.nextLine();
                datos = linea.split(";");
                for (int i = 0; i < datos.length - 1; i++) {
                    if (i == 0){
                        clAct = new Cliente(datos[i], datos[++i]);
                        this.anadirCliente(clAct);
                    } else {
                        if (datos[i + 3].equalsIgnoreCase("aperitivo")){
                            if (datos[i + 4].equalsIgnoreCase("bolsa")){
                                clAct.getArticulos().add(new Aperitivo(datos[i], datos[++i], Double.parseDouble(datos[++i]), datos[++i], Embalaje.BOLSA));
                            } else if (datos[i + 4].equalsIgnoreCase("break")){
                                clAct.getArticulos().add(new Aperitivo(datos[i], datos[++i], Double.parseDouble(datos[++i]), datos[++i], Embalaje.BREAK));
                            } else {
                                clAct.getArticulos().add(new Aperitivo(datos[i], datos[++i], Double.parseDouble(datos[++i]), datos[++i], Embalaje.LATA));
                            }
                        } else if (datos[i + 3].equalsIgnoreCase("bebida")){
                            clAct.getArticulos().add(new Bebida(datos[i], datos[++i], Double.parseDouble(datos[++i]), datos[++i], Double.parseDouble(datos[++i])));
                        } else {
                            clAct.getArticulos().add(new Vario(datos[i], datos[++i], Double.parseDouble(datos[++i]), datos[++i], Double.parseDouble(datos[++i])));
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ClienteYaExiste clienteYaExiste) {
            clienteYaExiste.printStackTrace();
        }
    }

    public void deListaACarpeta() throws NoEsCarpeta{
        if (!this.getCarpeta().exists()){
            this.getCarpeta().mkdir();
        }
        if (!this.getCarpeta().isDirectory()){
            throw new NoEsCarpeta(this.getCarpeta());
        }
        for (Cliente c :
                this.getClientes()) {
            File padre = new File(this.getCarpeta().getAbsolutePath() + "/" + c.getNombre());
            if (!padre.exists()){
                padre.mkdir();
            }
            if (!padre.isDirectory()){
                throw new NoEsCarpeta(padre);
            }
                for (Articulo art :
                        c.getArticulos()) {
                    File file = new File(padre.getAbsolutePath() + "/" + art.getId() + ".dat");

                    try {
                        if (file.exists()){
                            throw new FicheroExiste(file);
                        }
                        if (file.createNewFile()){
                            System.out.println("Archivo creado");
                        }
                        PrintWriter pw = new PrintWriter(file);
                        if (art instanceof Bebida){
                            Bebida beb = (Bebida) art;
                            pw.println(beb.getId() + ";" + beb.getNombre() + ";" + beb.getPrecio() + ";" + beb.getCategoria() + ";" + beb.getLitros() + ";");
                        } else if (art instanceof Aperitivo){
                            Aperitivo apr = (Aperitivo) art;
                            pw.println(apr.getId() + ";" + apr.getNombre() + ";" + apr.getPrecio() + ";" + apr.getCategoria() + ";" + apr.getTipo() + ";");
                        } else{
                            Vario vario = (Vario) art;
                            pw.println(vario.getId() + ";" + vario.getNombre() + ";" + vario.getPrecio() + ";" + vario.getCategoria() + ";" + vario.getKilos() + ";");
                        }
                        pw.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (FicheroExiste e){
                        System.out.println(e.toString());
                    }

            }
        }
    }

    public void deCarpetaACsv(){
        Scanner scanner;
        String linea;
        try {
            PrintWriter pw = new PrintWriter(archivo);
            for (File file :
                    carpeta.listFiles()) {
                if (file.isDirectory()) {
                    for (Cliente c :
                            this.getClientes()) {
                        if (c.getNombre().equalsIgnoreCase(file.getName()))
                            pw.print(c.getId() + ";" + c.getNombre() + ";");
                    }
                    for (File files:
                            file.listFiles()) {
                        scanner = new Scanner(files);
                        linea = scanner.nextLine();
                        pw.print(linea);
                    }
                }
                pw.println();
            }
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void imprimirRegistros(){
        System.out.println("Cod prov\tNom prov\tCod prod\tNom prod\tPrecio\tCaracteristicas");
        for (Cliente cl :
                this.getClientes()) {
            System.out.println(cl.getId() + "\t" + cl.getNombre() + "\t");;
            for (Articulo art :
                    cl.getArticulos()) {
                System.out.print("\t\t\t\t" + art.getId() + "\t" + art.getNombre() + "\t" + art.getPrecio() + "\t" + art.getCategoria() + "\t");
                if (art instanceof Bebida) {
                    Bebida bebida = (Bebida) art;
                    System.out.print(bebida.getLitros());
                } else if (art instanceof Aperitivo) {
                    Aperitivo aperitivo = (Aperitivo) art;
                    System.out.print(aperitivo.getTipo());
                } else {
                    Vario vario = (Vario) art;
                    System.out.print(vario.getKilos());
                }
                System.out.println();
            }
        }
    }

    public void anadirCliente(Cliente c) throws ClienteYaExiste{
        try {
            buscarCliente(c);
            this.getClientes().add(c);
        }catch (ClienteEncontrado e){
            throw new ClienteYaExiste(c);
        }
    }

    public void buscarCliente(Cliente c) throws ClienteEncontrado{
        for (Cliente cl :
                this.getClientes()) {
            if (cl.getId().equalsIgnoreCase(c.getId())) throw new ClienteEncontrado();
        }
    }

    public void borrarCliente(Cliente c) throws NoSePuedeBorrarCliente{
        if (c.getArticulos().size() > 0){
            throw new NoSePuedeBorrarCliente(c);
        } else borrar(c);
    }

    public void borrar(Cliente c){
        if (this.getClientes().size() == 1)
            this.getClientes().clear();
        else
            this.getClientes().remove(c);
    }

    public Cliente retornarCliente(String id){
        for (Cliente c :
                this.getClientes()) {
            if (c.getId().equalsIgnoreCase(id)) return c;
        }
        return null;
    }

    public void imprimirClientes(){
        for (Cliente c :
                this.getClientes()) {
            System.out.println(c);
        }
    }

    public File getArchivo() {
        return archivo;
    }

    public void setArchivo(File archivo) {
        this.archivo = archivo;
    }

    public File getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(File carpeta) {
        this.carpeta = carpeta;
    }

    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(ArrayList<Cliente> clientes) {
        this.clientes = clientes;
    }

    public void mostrarDisco(){

        System.out.println(this.carpeta.getName() + "/");
        for (File carpeta :
                this.carpeta.listFiles()) {
            if (carpeta.isDirectory()){
                System.out.println("\t\t\t" + carpeta.getName() + "/");
                for (File files :
                        carpeta.listFiles()) {
                    System.out.println("\t\t\t\t\t\t" + files.getName());
                }
            }
        }
    }
}
