public class Vario extends Articulo {

    private double kilos;

    public Vario(String id, String nombre, double precio, String categoria, double kilos) {
        super(id, nombre, precio, categoria);
        this.kilos = kilos;
    }



    public double getKilos() {
        return kilos;
    }

    public void setKilos(double kilos) {
        this.kilos = kilos;
    }

    @Override
    public int calcularNumeroCajas() {
        return (int) Math.round(this.kilos / 20) > 0?(int) Math.round(this.kilos / 20):1;
    }
}
